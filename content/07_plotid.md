---
title: 'PlotID - A framework for tracking figures'
status: working
task: [A-2-2, A-2-3, A-2-5, A-3-2] # includes measure
is-published: Yes # Yes, No
internal: No # Yes, No
tags: [Alex,NFDI4ing,
  MATLAB, NFDI4ing, Data Management, Identifier, PlotID]
statusreport_ids: 19 # ID numbers used in NFDI4ing Status Report (Statusbericht) that this article relates to
authors:
  - name: Jan Lemmer^[corresponding author]
    orcid: 0000-0002-0638-1567
    affiliation: 1 # (Multiple affiliations must be quoted)
  - name: Martin Hock
    orcid: 0000-0001-9917-3152
    affiliation: 1
  - name: Manuela Richter
    orcid: 0000-0003-1060-2622
    affiliation: 1
affiliations:
 - name: Chair of Fluid Systems, TU Darmstadt
   index: 1
date: 2021-10-14
---
# Demo Implementation of the Toolkit PlotID

## Summary
The plotID toolkit supports researchers in tracking and storing relevant data in plots. Plots are labeled with an ID and the corresponding data is stored depending on the researcher's need.

The Software is Open Source and the repository is available at [GitLab](https://git.rwth-aachen.de/plotid).

**Measures:**
A-2-2, A-2-3, A-2-5, A-3-2
## Description
Figure, plots and diagrams are key for presenting condensed knowledge in the scientific world. However, it is often difficult or impossible to understand which data are shown in an image or and how they were processed. PlotID helbs you in two steps. First the plot is labeled with an ID of your choise and second the research data and the plotting script is exported in a folder with this ID. This makes it possible to reproduce the workflow in future.
 
<!-- ![Schematic showing the three steps of creating a plot with data and code, tagging the plot with an ID collecting all components for archiving or sharing.](methodology.png)  -->

{{% figure src="methodology.png" title="Methodology of plotID" alt="Schematic showing the three steps of creating a plot with data and code, tagging the plot with an ID collecting all components for archiving or sharing."%}}

### Status
#### planned activities

- Investigation about tools plotID could be integrated with (software workflows, JupyterLab Notebooks, data validation pipelines...)

#### in progress activities

- We are now working hard on MATLAB V2.0 with many additional Features
- Development of a python version started in Q2 2022
- Starting October 2021, the matlab version has been used in pilot projects by scientists and students

#### completed activities
- A stable release is published at [Zenodo](https://doi.org/10.5281/zenodo.5714480) in Q1 2022
- Implementation in MATLAB, which fullfills the basic user needs. Q4 2021

## Results
- a simple CI/CD test case is implemented
- toolkit which makes referencing plots easy and enables the export of the research data

The release of PlotID V1 is available on [GitLab](https://git.rwth-aachen.de/plotid), feel free to participate.

### Lessons Learned/ Recommendations

- usability is crucial for user acceptance
- ease of use requires a lot of user feedback
- object oriented programming should be used, if a lot of communication between functions is neccesary

### Publication(s)

For a detailed documentation, see the [GitLab group](https://git.rwth-aachen.de/plotid) of PlotID.

## Acknowledgements
We acknowledge contributions from the NFDI4ing Team @TU Darmstadt and the pilot users.
